<?php

namespace App\Services;

use App\Exports\UserExport;
use App\Models\User;
use App\Repositories\UserRepository;
use Maatwebsite\Excel\Facades\Excel;

class UserService
{
    public function __construct(private UserRepository $repo)
    {
    }

    public function getUser(array $data)
    {
        return $this->repo->getUser($data);
    }

    public function updateUser(User $user, array $data)
    {
        return $this->repo->updateUser($user, $data);
    }

    public function updateChecked(array $data)
    {
        return $this->repo->updateChecked($data);
    }

    public function export(array $data = [])
    {
        $fileName = 'user_export'.date('Ymd_His').'.xlsx';
        $users = $this->getUser($data);

        return Excel::download(new UserExport($users), $fileName);
    }
}
