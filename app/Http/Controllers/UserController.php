<?php

namespace App\Http\Controllers;

use App\Imports\ReadFileExcel;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    public function __construct(private UserService $service)
    {
    }

    public function readFileExcel(Request $request)
    {
        $request->validate([
            'file' => 'required|file|mimes:xlsx,xls|max:2048',
        ]);

        $dataReader = [];
        Excel::import(new ReadFileExcel(function ($data) use (&$dataReader) {
            $dataReader = $data;
        }), $request->file);

        $users = $this->service->getUser($dataReader->toArray());

        return view('list-user', ['users' => $users]);
    }

    public function updateUser(User $user, Request $request)
    {
        $data = $request->all();

        $this->service->updateUser($user, $data);

        return response()->json(['message' => 'Users updated successfully'], Response::HTTP_OK);
    }

    public function updateChecked(Request $request)
    {
        $data = $request->all();

        $this->service->updateChecked($data);

        return response()->json(['message' => 'Users updated successfully'], Response::HTTP_OK);
    }

    public function exportExcel(Request $request)
    {
        $userIds = $request->all('id');

        return $this->service->export($userIds['id'] ?? []);

        // return response()->json(['message' => 'Users updated successfully'], Response::HTTP_OK);
    }
}
